﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

using Hont;
using System.Collections.ObjectModel;

namespace Hont.AStar
{
    public class HontAStarUnityOCTTreeDebuger : MonoBehaviour
    {
        public HontAStarUnity astar;


        [ContextMenu("Execute In Editor")]
        void OnEnable()
        {
            if (astar == null) return;

            var list = new HashSet<Position>();

            int totalItemCount = 0;
            Action<ReadOnlyCollection<OCTNode<Position>>> r = null;

            r = (collection) =>
            {
                foreach (var node in collection)
                {
                    totalItemCount += node.Items.Count;

                    foreach (var item in node.Items)
                    {
                        list.Add(item.Value);
                    }

                    if (node.HasChildren)
                        r(node.Childrens);
                }
            };

            r(astar.OctTree.Root.Childrens);

            foreach (Position item in astar.Grid)
            {
                if (!list.Contains(item))
                {
                    Debug.Log("item: " + item);
                }
            }

            Debug.Log(" list: " + list.Count + " totalItemCount: " + totalItemCount);
        }
    }
}
