﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Hont.AStar
{
    [RequireComponent(typeof(HontAStarUnity))]
    public class HontAStarUnityStepDebuger : MonoBehaviour, IAStarStepDebuger
    {
        public class StepInfo
        {
            public Position AStarPos;
            public DebugerNodeTypeEnum NodeType;
        }

        [HideInInspector]
        public int currentStep;
        public HontAStarUnity astar;

        List<StepInfo> mStepList;
        Position mStartPos;
        Position mEndPos;

        public List<StepInfo> StepList { get { return mStepList; } }
        public Position StartPosition { get { return mStartPos; } }
        public Position EndPosition { get { return mEndPos; } }


        public void Init()
        {
            mStepList = new List<StepInfo>();
            astar.Debuger = this;
        }

        public void OnPathfindingBegin(Position startPos, Position endPos)
        {
            this.currentStep = 0;
            this.mStartPos = startPos;
            this.mEndPos = endPos;

            mStepList.Clear();
        }

        public void OnToNextPoint(Position nextPoint, DebugerNodeTypeEnum nodeType)
        {
            mStepList.Add(new StepInfo() { AStarPos = nextPoint, NodeType = nodeType });
        }

        public void OnAddToClosedList(Position point)
        {
            mStepList.Add(new StepInfo() { AStarPos = point, NodeType = DebugerNodeTypeEnum.CloseNode });
        }
    }
}
